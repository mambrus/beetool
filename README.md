# beetool

## Description

`beetool` is a set of relatively few CLI pipe-components each preforming a
simple task, but when put together in various compositions and attributes
interconnected with pipes, can be used to change formats of binary blobs of
data without the need to use sledgehammers like high-level languages like
Pearl, Python or C (neither really suited for the task).

Although not fully possible, it's meant to be to binary data what `sed`,
`awk`, `tee` and `paste` is to text.

Project is inspired by the obvious lack, and fully in agreement with the
following discussion - 10y ago at the date of this writing (!):

* [thread](https://unix.stackexchange.com/questions/118969/a-shell-like-environment-for-binary-processing)


## Installation

TBD


## Usage

* `btee` - Split an in-file. Similar to `tee` but each output is chopped
  into repeated chunks of the input
* `bjoin` - The opposite to `btee`, i.e. join in-files. Besides output from
  `beetool` components, existing tools like `dd` are suitable as input sources
* `btrans` - Transform chunks of input into output, potentially either
  shrinking or growing the output. The transforms are either
  * simple operations like `shift`, `div`, `mul` given as options
  * General mathematical expressions (TBD)
  * User-supplied transform provided as a shared-library to the CLI

### See also

Where `beetools` don't take you all the way, the folloing could help cover the gaps:

* [bbe](https://linux.die.net/man/1/bbe)
* [dd](https://linux.die.net/man/1/dd)

## Support

File a ticket


## Contributing

Suggestions, translations, man-pages, review


## Authors and acknowledgment

Michael Ambrus <michael@ambrus.se>


## License

GPL v2

## Copyright

Authors


## Project status

Planning

